package org.mineacademy.bungeechatapi;

import org.bukkit.plugin.java.JavaPlugin;

public class BungeeChatAPI extends JavaPlugin {

	@Override
	public void onLoad() {
		getLogger().info("Loaded net.md_5.bungee and com.google.gson libraries.");
		getLogger().info("Credit goes to https://github.com/SpigotMC/BungeeCord");
	}

	@Override
	public void onEnable() {
	}
}
